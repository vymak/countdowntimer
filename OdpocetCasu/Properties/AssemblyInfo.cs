﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("OdpocetCasu")]
[assembly: AssemblyDescription("Aplikace sloužící k odpočtu času do určitého datumu, dále bude postupně implementována nová funkčnost")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Vymakdevel")]
[assembly: AssemblyProduct("OdpocetCasu")]
[assembly: AssemblyCopyright("Copyright © Vymak 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("b53feab1-1cfb-41fd-8f3b-079c43fdfcf8")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.9.8.16")]
[assembly: AssemblyFileVersion("0.9.8.16")]
