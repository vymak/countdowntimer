﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;

namespace OdpocetCasu.Classes
{
    public class UpdateChecker
    {
        /// <summary>
        /// Zjištění nejnovější verze z internetu
        /// </summary>
        /// <returns>String</returns>
        internal static string GetLatestVersionFromNet()
        {
            try
            {
                WebClient wc = new WebClient();
                return wc.DownloadString("http://vym0008.vymak.cz/download/timer/verze.txt");
            }
            catch (WebException)
            {
                return GetVersion().ToString();
            }
        }

        /// <summary>
        /// Indikuje zda-li je dostupná novější verze programu
        /// </summary>
        /// <returns>Bool</returns>
        internal static bool IsNewVersionAvailible()
        {
            if (GetLatestVersionFromNet() == UpdateChecker.GetVersion().ToString())
                return false;
            else
                return true;
        }

        /// <summary>
        /// Vrácení verze assembly
        /// </summary>
        /// <returns>Version</returns>
        public static Version GetVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version;
        }

        /// <summary>
        /// Metoda obstarávající stažení aktuální verze
        /// </summary>
        /// <param name="path">Cesta k uložení</param>
        internal static bool DownloadNewVersion(string path)
        {
            try
            {
                WebClient wc = new WebClient();
                wc.DownloadFile(new Uri("http://vym0008.vymak.cz/download/timer/OdpocetCasu.exe"), path);

                FileInfo file = new FileInfo(path);

                if (file.Length != 0)
                    return true;
                else
                {
                    try
                    {
                        file.Delete();
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                    return false;
                }
            }
            catch (WebException)
            {
                return false;
            }
        }

    }
}
