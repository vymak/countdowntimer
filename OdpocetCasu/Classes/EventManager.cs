﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace OdpocetCasu.Classes
{
    internal class EventManager
    {
        /// <summary>
        /// Uložení seznamu na disk
        /// </summary>
        /// <param name="list">List událostí</param>
        internal static void SaveEvents(Dictionary<DateTime, string> list)
        {
            using (StreamWriter str = new StreamWriter("data.bin"))
            {
                foreach (var item in list)
                {
                    string crypted = item.Key.ToString() + ";" + item.Value;
                    str.WriteLine(AESEncryption.Encrypt(crypted, "k4dz4H-owUzIMwmjt9vj"));
                }
            }
        }

        /// <summary>
        /// Načtení událostí ze souboru
        /// </summary>
        /// <returns>Dictionary</returns>
        internal static Dictionary<DateTime, string> LoadEventsFromFile()
        {
            Dictionary<DateTime, string> list = new Dictionary<DateTime, string>();

            using (StreamReader str = new StreamReader("data.bin"))
            {
                string line;
                while ((line = str.ReadLine()) != null)
                {
                    try
                    {
                        string decryptedline = AESEncryption.Decrypt(line, "k4dz4H-owUzIMwmjt9vj");
                        string[] data = decryptedline.Split(';');

                        try
                        {
                            DateTime datum = DateTime.Parse(data[0]);
                            list.Add(datum, data[1]);
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                    }
                    catch (CryptographicException)
                    {
                        continue;
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// Smazání řádku podle datumu
        /// </summary>
        /// <param name="datum">Datum ke smazání</param>
        /// <param name="list">List událostí</param>
        /// <returns>Dictionary</returns>
        internal static Dictionary<DateTime, string> RemoveRowByIndex(string datum, Dictionary<DateTime, string> list)
        {
            Dictionary<DateTime, string> tmp = new Dictionary<DateTime, string>();

            foreach (var item in list)
            {
                if (item.Key.ToString() != datum)
                    tmp.Add(item.Key, item.Value);
            }

            return tmp;
        }

        /// <summary>
        /// Vytvoření string pro výpis
        /// </summary>
        /// <param name="rozdil">TimeSpan rozdil</param>
        /// <returns>String</returns>
        internal static string CreateTimeSpanString(TimeSpan rozdil)
        {
            StringBuilder doba = new StringBuilder();

            // dny
            if (rozdil.Days > 0)
            {
                if (rozdil.Days > 1)
                    doba.Append(rozdil.Days.ToString() + " dní ");
                else
                    doba.Append(rozdil.Days.ToString() + " den ");
            }
            
            // hodiny
            if (rozdil.Hours > 0)
            {
                if (rozdil.Hours > 1)
                    doba.Append(rozdil.Hours.ToString() + " hodin ");
                else
                    doba.Append(rozdil.Hours.ToString() + " hodina ");
            }

            // minuty
            if (rozdil.Minutes != 1)
                doba.Append(rozdil.Minutes.ToString() + " minut ");
            else
                doba.Append(rozdil.Minutes.ToString() + " minuta ");

            // vteřiny
            if (rozdil.Seconds != 1)
                doba.Append(rozdil.Seconds.ToString() + " vteřin ");
            else
                doba.Append(rozdil.Seconds.ToString() + " vteřina ");


            return doba.ToString();
        }
    }
}
