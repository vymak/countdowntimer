﻿namespace OdpocetCasu
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.souborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oProgramuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.konecToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.správaUdálostíToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nastaveníToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aktualizovatAutomatickyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.odeslatFeedBackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.timerChceckbox = new System.Windows.Forms.CheckBox();
            this.topCheckbox = new System.Windows.Forms.CheckBox();
            this.hodinyVyber = new System.Windows.Forms.DomainUpDown();
            this.minutyVyber = new System.Windows.Forms.DomainUpDown();
            this.vterinyVyber = new System.Windows.Forms.DomainUpDown();
            this.nazevUdalosti = new System.Windows.Forms.TextBox();
            this.vlozButton = new System.Windows.Forms.Button();
            this.vyberDatum = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.kontrolaTimer = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.alarmLabel = new System.Windows.Forms.LinkLabel();
            this.stopwatchLabel = new System.Windows.Forms.LinkLabel();
            this.countdownLabel = new System.Windows.Forms.LinkLabel();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.aktualniDatum = new System.Windows.Forms.Label();
            this.ciloveDatum = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.nazevlabel = new System.Windows.Forms.Label();
            this.nazevNadpis = new System.Windows.Forms.Label();
            this.playSound = new System.Windows.Forms.CheckBox();
            this.zbyvajiciCas = new System.Windows.Forms.Label();
            this.nastaveniIntervalu = new System.Windows.Forms.TrackBar();
            this.intervalLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nastaveniIntervalu)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3});
            this.statusStrip1.Location = new System.Drawing.Point(0, 365);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(764, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(89, 17);
            this.toolStripStatusLabel1.Text = "Libor Vymětalík";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(10, 17);
            this.toolStripStatusLabel2.Text = "|";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel3.Text = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.souborToolStripMenuItem,
            this.správaUdálostíToolStripMenuItem,
            this.nastaveníToolStripMenuItem,
            this.odeslatFeedBackToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(764, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // souborToolStripMenuItem
            // 
            this.souborToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oProgramuToolStripMenuItem,
            this.toolStripSeparator1,
            this.konecToolStripMenuItem});
            this.souborToolStripMenuItem.Image = global::OdpocetCasu.Properties.Resources._1366587733_2x2_grid;
            this.souborToolStripMenuItem.Name = "souborToolStripMenuItem";
            this.souborToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.souborToolStripMenuItem.Text = "Soubor";
            // 
            // oProgramuToolStripMenuItem
            // 
            this.oProgramuToolStripMenuItem.Image = global::OdpocetCasu.Properties.Resources._1366587407_font_italic;
            this.oProgramuToolStripMenuItem.Name = "oProgramuToolStripMenuItem";
            this.oProgramuToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.oProgramuToolStripMenuItem.Text = "O programu";
            this.oProgramuToolStripMenuItem.Click += new System.EventHandler(this.MenuManage);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(136, 6);
            // 
            // konecToolStripMenuItem
            // 
            this.konecToolStripMenuItem.Image = global::OdpocetCasu.Properties.Resources._1366587347_on_off;
            this.konecToolStripMenuItem.Name = "konecToolStripMenuItem";
            this.konecToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.konecToolStripMenuItem.Text = "Konec";
            this.konecToolStripMenuItem.Click += new System.EventHandler(this.MenuManage);
            // 
            // správaUdálostíToolStripMenuItem
            // 
            this.správaUdálostíToolStripMenuItem.Image = global::OdpocetCasu.Properties.Resources._1366587367_calendar_2;
            this.správaUdálostíToolStripMenuItem.Name = "správaUdálostíToolStripMenuItem";
            this.správaUdálostíToolStripMenuItem.Size = new System.Drawing.Size(115, 20);
            this.správaUdálostíToolStripMenuItem.Text = "Správa událostí";
            this.správaUdálostíToolStripMenuItem.Click += new System.EventHandler(this.VyberDatum_Click);
            // 
            // nastaveníToolStripMenuItem
            // 
            this.nastaveníToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aktualizovatAutomatickyToolStripMenuItem});
            this.nastaveníToolStripMenuItem.Image = global::OdpocetCasu.Properties.Resources._1366587838_wrench;
            this.nastaveníToolStripMenuItem.Name = "nastaveníToolStripMenuItem";
            this.nastaveníToolStripMenuItem.Size = new System.Drawing.Size(87, 20);
            this.nastaveníToolStripMenuItem.Text = "Nastavení";
            // 
            // aktualizovatAutomatickyToolStripMenuItem
            // 
            this.aktualizovatAutomatickyToolStripMenuItem.Checked = true;
            this.aktualizovatAutomatickyToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.aktualizovatAutomatickyToolStripMenuItem.Image = global::OdpocetCasu.Properties.Resources._1366587780_playback_reload;
            this.aktualizovatAutomatickyToolStripMenuItem.Name = "aktualizovatAutomatickyToolStripMenuItem";
            this.aktualizovatAutomatickyToolStripMenuItem.Size = new System.Drawing.Size(282, 22);
            this.aktualizovatAutomatickyToolStripMenuItem.Text = "Aktualizovat soubor s daty automaticky";
            this.aktualizovatAutomatickyToolStripMenuItem.Click += new System.EventHandler(this.MenuManage);
            // 
            // odeslatFeedBackToolStripMenuItem
            // 
            this.odeslatFeedBackToolStripMenuItem.Image = global::OdpocetCasu.Properties.Resources._1366587668_bug;
            this.odeslatFeedBackToolStripMenuItem.Name = "odeslatFeedBackToolStripMenuItem";
            this.odeslatFeedBackToolStripMenuItem.Size = new System.Drawing.Size(128, 20);
            this.odeslatFeedBackToolStripMenuItem.Text = "Odeslat FeedBack";
            this.odeslatFeedBackToolStripMenuItem.Click += new System.EventHandler(this.MenuManage);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 30;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // timerChceckbox
            // 
            this.timerChceckbox.AutoSize = true;
            this.timerChceckbox.Location = new System.Drawing.Point(192, 290);
            this.timerChceckbox.Name = "timerChceckbox";
            this.timerChceckbox.Size = new System.Drawing.Size(100, 17);
            this.timerChceckbox.TabIndex = 9;
            this.timerChceckbox.Text = "Pozastavit timer";
            this.toolTip1.SetToolTip(this.timerChceckbox, "Pozastaví aktualizaci času");
            this.timerChceckbox.UseVisualStyleBackColor = true;
            this.timerChceckbox.CheckedChanged += new System.EventHandler(this.CheckBoxManage);
            // 
            // topCheckbox
            // 
            this.topCheckbox.AutoSize = true;
            this.topCheckbox.Location = new System.Drawing.Point(297, 290);
            this.topCheckbox.Name = "topCheckbox";
            this.topCheckbox.Size = new System.Drawing.Size(86, 17);
            this.topCheckbox.TabIndex = 10;
            this.topCheckbox.Text = "Vždy nahoře";
            this.toolTip1.SetToolTip(this.topCheckbox, "Zobrazit okno vždy nad ostatními okny");
            this.topCheckbox.UseVisualStyleBackColor = true;
            this.topCheckbox.CheckedChanged += new System.EventHandler(this.CheckBoxManage);
            // 
            // hodinyVyber
            // 
            this.hodinyVyber.Items.Add("23");
            this.hodinyVyber.Items.Add("22");
            this.hodinyVyber.Items.Add("21");
            this.hodinyVyber.Items.Add("20");
            this.hodinyVyber.Items.Add("19");
            this.hodinyVyber.Items.Add("18");
            this.hodinyVyber.Items.Add("17");
            this.hodinyVyber.Items.Add("16");
            this.hodinyVyber.Items.Add("15");
            this.hodinyVyber.Items.Add("14");
            this.hodinyVyber.Items.Add("13");
            this.hodinyVyber.Items.Add("12");
            this.hodinyVyber.Items.Add("11");
            this.hodinyVyber.Items.Add("10");
            this.hodinyVyber.Items.Add("9");
            this.hodinyVyber.Items.Add("8");
            this.hodinyVyber.Items.Add("7");
            this.hodinyVyber.Items.Add("6");
            this.hodinyVyber.Items.Add("5");
            this.hodinyVyber.Items.Add("4");
            this.hodinyVyber.Items.Add("3");
            this.hodinyVyber.Items.Add("2");
            this.hodinyVyber.Items.Add("1");
            this.hodinyVyber.Items.Add("0");
            this.hodinyVyber.Location = new System.Drawing.Point(559, 210);
            this.hodinyVyber.Name = "hodinyVyber";
            this.hodinyVyber.ReadOnly = true;
            this.hodinyVyber.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.hodinyVyber.Size = new System.Drawing.Size(45, 20);
            this.hodinyVyber.TabIndex = 16;
            this.toolTip1.SetToolTip(this.hodinyVyber, "Zadejte hodinu");
            this.hodinyVyber.SelectedItemChanged += new System.EventHandler(this.NastaveniCasu);
            // 
            // minutyVyber
            // 
            this.minutyVyber.Items.Add("59");
            this.minutyVyber.Items.Add("58");
            this.minutyVyber.Items.Add("57");
            this.minutyVyber.Items.Add("56");
            this.minutyVyber.Items.Add("55");
            this.minutyVyber.Items.Add("54");
            this.minutyVyber.Items.Add("53");
            this.minutyVyber.Items.Add("52");
            this.minutyVyber.Items.Add("51");
            this.minutyVyber.Items.Add("50");
            this.minutyVyber.Items.Add("49");
            this.minutyVyber.Items.Add("48");
            this.minutyVyber.Items.Add("47");
            this.minutyVyber.Items.Add("46");
            this.minutyVyber.Items.Add("45");
            this.minutyVyber.Items.Add("44");
            this.minutyVyber.Items.Add("43");
            this.minutyVyber.Items.Add("42");
            this.minutyVyber.Items.Add("41");
            this.minutyVyber.Items.Add("40");
            this.minutyVyber.Items.Add("39");
            this.minutyVyber.Items.Add("38");
            this.minutyVyber.Items.Add("37");
            this.minutyVyber.Items.Add("36");
            this.minutyVyber.Items.Add("35");
            this.minutyVyber.Items.Add("34");
            this.minutyVyber.Items.Add("33");
            this.minutyVyber.Items.Add("32");
            this.minutyVyber.Items.Add("31");
            this.minutyVyber.Items.Add("30");
            this.minutyVyber.Items.Add("29");
            this.minutyVyber.Items.Add("28");
            this.minutyVyber.Items.Add("27");
            this.minutyVyber.Items.Add("26");
            this.minutyVyber.Items.Add("25");
            this.minutyVyber.Items.Add("24");
            this.minutyVyber.Items.Add("23");
            this.minutyVyber.Items.Add("22");
            this.minutyVyber.Items.Add("21");
            this.minutyVyber.Items.Add("20");
            this.minutyVyber.Items.Add("19");
            this.minutyVyber.Items.Add("18");
            this.minutyVyber.Items.Add("17");
            this.minutyVyber.Items.Add("16");
            this.minutyVyber.Items.Add("15");
            this.minutyVyber.Items.Add("14");
            this.minutyVyber.Items.Add("13");
            this.minutyVyber.Items.Add("12");
            this.minutyVyber.Items.Add("11");
            this.minutyVyber.Items.Add("10");
            this.minutyVyber.Items.Add("9");
            this.minutyVyber.Items.Add("8");
            this.minutyVyber.Items.Add("7");
            this.minutyVyber.Items.Add("6");
            this.minutyVyber.Items.Add("5");
            this.minutyVyber.Items.Add("4");
            this.minutyVyber.Items.Add("3");
            this.minutyVyber.Items.Add("2");
            this.minutyVyber.Items.Add("1");
            this.minutyVyber.Items.Add("0");
            this.minutyVyber.Location = new System.Drawing.Point(621, 210);
            this.minutyVyber.Name = "minutyVyber";
            this.minutyVyber.ReadOnly = true;
            this.minutyVyber.Size = new System.Drawing.Size(45, 20);
            this.minutyVyber.TabIndex = 17;
            this.toolTip1.SetToolTip(this.minutyVyber, "Zadejte minuty");
            this.minutyVyber.SelectedItemChanged += new System.EventHandler(this.NastaveniCasu);
            // 
            // vterinyVyber
            // 
            this.vterinyVyber.Items.Add("59");
            this.vterinyVyber.Items.Add("58");
            this.vterinyVyber.Items.Add("57");
            this.vterinyVyber.Items.Add("56");
            this.vterinyVyber.Items.Add("55");
            this.vterinyVyber.Items.Add("54");
            this.vterinyVyber.Items.Add("53");
            this.vterinyVyber.Items.Add("52");
            this.vterinyVyber.Items.Add("51");
            this.vterinyVyber.Items.Add("50");
            this.vterinyVyber.Items.Add("49");
            this.vterinyVyber.Items.Add("48");
            this.vterinyVyber.Items.Add("47");
            this.vterinyVyber.Items.Add("46");
            this.vterinyVyber.Items.Add("45");
            this.vterinyVyber.Items.Add("44");
            this.vterinyVyber.Items.Add("43");
            this.vterinyVyber.Items.Add("42");
            this.vterinyVyber.Items.Add("41");
            this.vterinyVyber.Items.Add("40");
            this.vterinyVyber.Items.Add("39");
            this.vterinyVyber.Items.Add("38");
            this.vterinyVyber.Items.Add("37");
            this.vterinyVyber.Items.Add("36");
            this.vterinyVyber.Items.Add("35");
            this.vterinyVyber.Items.Add("34");
            this.vterinyVyber.Items.Add("33");
            this.vterinyVyber.Items.Add("32");
            this.vterinyVyber.Items.Add("31");
            this.vterinyVyber.Items.Add("30");
            this.vterinyVyber.Items.Add("29");
            this.vterinyVyber.Items.Add("28");
            this.vterinyVyber.Items.Add("27");
            this.vterinyVyber.Items.Add("26");
            this.vterinyVyber.Items.Add("25");
            this.vterinyVyber.Items.Add("24");
            this.vterinyVyber.Items.Add("23");
            this.vterinyVyber.Items.Add("22");
            this.vterinyVyber.Items.Add("21");
            this.vterinyVyber.Items.Add("20");
            this.vterinyVyber.Items.Add("19");
            this.vterinyVyber.Items.Add("18");
            this.vterinyVyber.Items.Add("17");
            this.vterinyVyber.Items.Add("16");
            this.vterinyVyber.Items.Add("15");
            this.vterinyVyber.Items.Add("14");
            this.vterinyVyber.Items.Add("13");
            this.vterinyVyber.Items.Add("12");
            this.vterinyVyber.Items.Add("11");
            this.vterinyVyber.Items.Add("10");
            this.vterinyVyber.Items.Add("9");
            this.vterinyVyber.Items.Add("8");
            this.vterinyVyber.Items.Add("7");
            this.vterinyVyber.Items.Add("6");
            this.vterinyVyber.Items.Add("5");
            this.vterinyVyber.Items.Add("4");
            this.vterinyVyber.Items.Add("3");
            this.vterinyVyber.Items.Add("2");
            this.vterinyVyber.Items.Add("1");
            this.vterinyVyber.Items.Add("0");
            this.vterinyVyber.Location = new System.Drawing.Point(686, 210);
            this.vterinyVyber.Name = "vterinyVyber";
            this.vterinyVyber.ReadOnly = true;
            this.vterinyVyber.Size = new System.Drawing.Size(45, 20);
            this.vterinyVyber.TabIndex = 18;
            this.toolTip1.SetToolTip(this.vterinyVyber, "Zadejte vteřiny");
            this.vterinyVyber.SelectedItemChanged += new System.EventHandler(this.NastaveniCasu);
            // 
            // nazevUdalosti
            // 
            this.nazevUdalosti.Location = new System.Drawing.Point(545, 304);
            this.nazevUdalosti.MaxLength = 15;
            this.nazevUdalosti.Name = "nazevUdalosti";
            this.nazevUdalosti.Size = new System.Drawing.Size(105, 20);
            this.nazevUdalosti.TabIndex = 19;
            this.toolTip1.SetToolTip(this.nazevUdalosti, "Zadejte název události");
            this.nazevUdalosti.TextChanged += new System.EventHandler(this.NazevUdalosti_TextChanged);
            // 
            // vlozButton
            // 
            this.vlozButton.Enabled = false;
            this.vlozButton.Location = new System.Drawing.Point(656, 302);
            this.vlozButton.Name = "vlozButton";
            this.vlozButton.Size = new System.Drawing.Size(75, 23);
            this.vlozButton.TabIndex = 22;
            this.vlozButton.Text = "Uložit datum";
            this.toolTip1.SetToolTip(this.vlozButton, "Vloží novou událost do seznamu, nedojde k uložení na disk");
            this.vlozButton.UseVisualStyleBackColor = true;
            this.vlozButton.Click += new System.EventHandler(this.VlozitButtonClick);
            // 
            // vyberDatum
            // 
            this.vyberDatum.Location = new System.Drawing.Point(656, 273);
            this.vyberDatum.Name = "vyberDatum";
            this.vyberDatum.Size = new System.Drawing.Size(75, 23);
            this.vyberDatum.TabIndex = 23;
            this.vyberDatum.Text = "Správa dat";
            this.toolTip1.SetToolTip(this.vyberDatum, "Správce zadaných událostí");
            this.vyberDatum.UseVisualStyleBackColor = true;
            this.vyberDatum.Click += new System.EventHandler(this.VyberDatum_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Spouštěcí soubor |*.exe";
            // 
            // kontrolaTimer
            // 
            this.kontrolaTimer.Interval = 1000;
            this.kontrolaTimer.Tick += new System.EventHandler(this.kontrolaTimer_Tick);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SteelBlue;
            this.panel1.Controls.Add(this.alarmLabel);
            this.panel1.Controls.Add(this.stopwatchLabel);
            this.panel1.Controls.Add(this.countdownLabel);
            this.panel1.Location = new System.Drawing.Point(0, 23);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(183, 342);
            this.panel1.TabIndex = 28;
            // 
            // alarmLabel
            // 
            this.alarmLabel.AutoSize = true;
            this.alarmLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.alarmLabel.LinkColor = System.Drawing.Color.White;
            this.alarmLabel.Location = new System.Drawing.Point(12, 177);
            this.alarmLabel.Name = "alarmLabel";
            this.alarmLabel.Size = new System.Drawing.Size(61, 25);
            this.alarmLabel.TabIndex = 2;
            this.alarmLabel.TabStop = true;
            this.alarmLabel.Text = "Budík";
            this.alarmLabel.Visible = false;
            this.alarmLabel.VisitedLinkColor = System.Drawing.Color.White;
            // 
            // stopwatchLabel
            // 
            this.stopwatchLabel.AutoSize = true;
            this.stopwatchLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.stopwatchLabel.LinkColor = System.Drawing.Color.White;
            this.stopwatchLabel.Location = new System.Drawing.Point(12, 114);
            this.stopwatchLabel.Name = "stopwatchLabel";
            this.stopwatchLabel.Size = new System.Drawing.Size(73, 25);
            this.stopwatchLabel.TabIndex = 1;
            this.stopwatchLabel.TabStop = true;
            this.stopwatchLabel.Text = "Stopky";
            this.stopwatchLabel.Visible = false;
            this.stopwatchLabel.VisitedLinkColor = System.Drawing.Color.White;
            // 
            // countdownLabel
            // 
            this.countdownLabel.AutoSize = true;
            this.countdownLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.countdownLabel.LinkColor = System.Drawing.Color.White;
            this.countdownLabel.Location = new System.Drawing.Point(12, 51);
            this.countdownLabel.Name = "countdownLabel";
            this.countdownLabel.Size = new System.Drawing.Size(134, 25);
            this.countdownLabel.TabIndex = 0;
            this.countdownLabel.TabStop = true;
            this.countdownLabel.Text = "Odpočet času";
            this.countdownLabel.VisitedLinkColor = System.Drawing.Color.White;
            this.countdownLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LabelMenuClick);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.BackColor = System.Drawing.SystemColors.Control;
            this.monthCalendar1.FirstDayOfWeek = System.Windows.Forms.Day.Monday;
            this.monthCalendar1.Location = new System.Drawing.Point(559, 39);
            this.monthCalendar1.MaxDate = new System.DateTime(2099, 12, 31, 0, 0, 0, 0);
            this.monthCalendar1.MinDate = new System.DateTime(2013, 4, 10, 0, 0, 0, 0);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.ShowTodayCircle = false;
            this.monthCalendar1.ShowWeekNumbers = true;
            this.monthCalendar1.TabIndex = 0;
            this.monthCalendar1.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.ZmenaData);
            // 
            // aktualniDatum
            // 
            this.aktualniDatum.AutoSize = true;
            this.aktualniDatum.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.aktualniDatum.Location = new System.Drawing.Point(212, 104);
            this.aktualniDatum.Name = "aktualniDatum";
            this.aktualniDatum.Size = new System.Drawing.Size(60, 24);
            this.aktualniDatum.TabIndex = 3;
            this.aktualniDatum.Text = "label1";
            // 
            // ciloveDatum
            // 
            this.ciloveDatum.AutoSize = true;
            this.ciloveDatum.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ciloveDatum.Location = new System.Drawing.Point(212, 167);
            this.ciloveDatum.Name = "ciloveDatum";
            this.ciloveDatum.Size = new System.Drawing.Size(60, 24);
            this.ciloveDatum.TabIndex = 4;
            this.ciloveDatum.Text = "label1";
            this.ciloveDatum.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(213, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Aktuální datum a čas:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(213, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Do zadaného datumu";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(213, 212);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "zbývá:";
            this.label4.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(606, 210);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 17);
            this.label5.TabIndex = 14;
            this.label5.Text = ":";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(668, 210);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = ":";
            // 
            // nazevlabel
            // 
            this.nazevlabel.AutoSize = true;
            this.nazevlabel.Location = new System.Drawing.Point(429, 307);
            this.nazevlabel.Name = "nazevlabel";
            this.nazevlabel.Size = new System.Drawing.Size(110, 13);
            this.nazevlabel.TabIndex = 20;
            this.nazevlabel.Text = "Název odpočítávadla";
            // 
            // nazevNadpis
            // 
            this.nazevNadpis.AutoSize = true;
            this.nazevNadpis.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.nazevNadpis.Location = new System.Drawing.Point(213, 39);
            this.nazevNadpis.Name = "nazevNadpis";
            this.nazevNadpis.Size = new System.Drawing.Size(172, 31);
            this.nazevNadpis.TabIndex = 21;
            this.nazevNadpis.Text = "nazevNadpis";
            this.nazevNadpis.Visible = false;
            // 
            // playSound
            // 
            this.playSound.AutoSize = true;
            this.playSound.Location = new System.Drawing.Point(192, 313);
            this.playSound.Name = "playSound";
            this.playSound.Size = new System.Drawing.Size(207, 17);
            this.playSound.TabIndex = 24;
            this.playSound.Text = "Přehrát zvuk při dokončení časovače";
            this.playSound.UseVisualStyleBackColor = true;
            // 
            // zbyvajiciCas
            // 
            this.zbyvajiciCas.AutoSize = true;
            this.zbyvajiciCas.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zbyvajiciCas.ForeColor = System.Drawing.Color.ForestGreen;
            this.zbyvajiciCas.Location = new System.Drawing.Point(213, 233);
            this.zbyvajiciCas.Name = "zbyvajiciCas";
            this.zbyvajiciCas.Size = new System.Drawing.Size(79, 29);
            this.zbyvajiciCas.TabIndex = 5;
            this.zbyvajiciCas.Text = "label1";
            this.zbyvajiciCas.Visible = false;
            // 
            // nastaveniIntervalu
            // 
            this.nastaveniIntervalu.AutoSize = false;
            this.nastaveniIntervalu.LargeChange = 50;
            this.nastaveniIntervalu.Location = new System.Drawing.Point(332, 336);
            this.nastaveniIntervalu.Maximum = 2000;
            this.nastaveniIntervalu.Minimum = 30;
            this.nastaveniIntervalu.Name = "nastaveniIntervalu";
            this.nastaveniIntervalu.Size = new System.Drawing.Size(119, 24);
            this.nastaveniIntervalu.SmallChange = 5;
            this.nastaveniIntervalu.TabIndex = 25;
            this.nastaveniIntervalu.TickStyle = System.Windows.Forms.TickStyle.None;
            this.nastaveniIntervalu.Value = 30;
            this.nastaveniIntervalu.Scroll += new System.EventHandler(this.NastaveniIntervalu_Scroll);
            // 
            // intervalLabel
            // 
            this.intervalLabel.AutoSize = true;
            this.intervalLabel.Location = new System.Drawing.Point(472, 349);
            this.intervalLabel.Name = "intervalLabel";
            this.intervalLabel.Size = new System.Drawing.Size(35, 13);
            this.intervalLabel.TabIndex = 26;
            this.intervalLabel.Text = "label1";
            this.intervalLabel.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(189, 342);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Frekvence aktualizace [ms]";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(764, 387);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.intervalLabel);
            this.Controls.Add(this.nastaveniIntervalu);
            this.Controls.Add(this.zbyvajiciCas);
            this.Controls.Add(this.playSound);
            this.Controls.Add(this.vyberDatum);
            this.Controls.Add(this.vlozButton);
            this.Controls.Add(this.nazevNadpis);
            this.Controls.Add(this.nazevlabel);
            this.Controls.Add(this.nazevUdalosti);
            this.Controls.Add(this.vterinyVyber);
            this.Controls.Add(this.minutyVyber);
            this.Controls.Add(this.hodinyVyber);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.topCheckbox);
            this.Controls.Add(this.timerChceckbox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ciloveDatum);
            this.Controls.Add(this.aktualniDatum);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.monthCalendar1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Aplikace pro odpočet času do určitého data";
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nastaveniIntervalu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem souborToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ToolStripMenuItem konecToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolStripMenuItem správaUdálostíToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramuToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        public System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ToolStripMenuItem nastaveníToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem aktualizovatAutomatickyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem odeslatFeedBackToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Timer kontrolaTimer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.LinkLabel alarmLabel;
        private System.Windows.Forms.LinkLabel stopwatchLabel;
        private System.Windows.Forms.LinkLabel countdownLabel;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label aktualniDatum;
        private System.Windows.Forms.Label ciloveDatum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox timerChceckbox;
        private System.Windows.Forms.CheckBox topCheckbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DomainUpDown hodinyVyber;
        private System.Windows.Forms.DomainUpDown minutyVyber;
        private System.Windows.Forms.DomainUpDown vterinyVyber;
        private System.Windows.Forms.TextBox nazevUdalosti;
        private System.Windows.Forms.Label nazevlabel;
        public System.Windows.Forms.Label nazevNadpis;
        public System.Windows.Forms.Button vlozButton;
        public System.Windows.Forms.Button vyberDatum;
        private System.Windows.Forms.CheckBox playSound;
        private System.Windows.Forms.Label zbyvajiciCas;
        private System.Windows.Forms.TrackBar nastaveniIntervalu;
        private System.Windows.Forms.Label intervalLabel;
        private System.Windows.Forms.Label label1;
    }
}

