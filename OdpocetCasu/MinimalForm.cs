﻿using OdpocetCasu.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OdpocetCasu
{
    /// <summary>
    /// Třída obsluhující form s minimálním zobrazením
    /// </summary>
    internal partial class MinimalForm : Form
    {
        /// <summary>
        /// Proměnná obsahující instanci hlavního okna
        /// </summary>
        private Form1 form;

        /// <summary>
        /// Konstruktor pro vytvoření nové instance okna
        /// </summary>
        /// <param name="form">Instance hlavního formu</param>
        internal MinimalForm(Form1 form)
        {
            InitializeComponent();
            this.form = form;
            this.timer1.Interval = this.form.timer1.Interval;
            this.Resize += MinimalForm_Resize;
            this.TopMost = true;

            if (form.NazevUdalosti != String.Empty)
                this.Text = "Odpočet do události: " + form.NazevUdalosti;
            else
                this.Text = form.PropertyZadaneDatum.ToString();

            int height = SystemInformation.PrimaryMonitorSize.Height;
            int width = SystemInformation.PrimaryMonitorSize.Width;

            this.Location = new Point(width - 330, height - 175);

            ciloveDatum.Text = form.PropertyZadaneDatum.ToString();
            obnovitToolStripMenuItem.Visible = false;

            this.timer1.Start();

            if (!this.form.timer1.Enabled)
                this.timer1.Stop();
        }

        /// <summary>
        /// Událost obsluhující změny stavu okna
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MinimalForm_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                form.Show();
                form.WindowState = FormWindowState.Normal;
                this.Close();
            }
            else if (this.WindowState == FormWindowState.Minimized)
            {
                obnovitToolStripMenuItem.Visible = true;
                this.Hide();
                notifyIcon1.ShowBalloonTip(5000, "Aplikace stále běží", "Aplikace byla minimalizována do panelu. Odpočet můžete kdykoliv obnovit z panelu.", ToolTipIcon.Info);
            }
        }

        /// <summary>
        /// Událost obsluhující tick timeru
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer1_Tick(object sender, EventArgs e)
        {
            label1.Visible = true;

            ciloveDatum.Text = form.PropertyZadaneDatum.ToString();
            label1.Text = null;

            TimeSpan test = form.PropertyZadaneDatum - DateTime.Now;

            string doba = EventManager.CreateTimeSpanString(test);
            label1.Text = doba;

            if (this.WindowState == FormWindowState.Minimized)
            {
                if (test.Minutes % 30 == 0 && test.Seconds == 0)
                    notifyIcon1.ShowBalloonTip(20000, "Událost " + form.NazevUdalosti, "Do konce odpočtu zbývá ještě: " + doba, ToolTipIcon.Info);
            }

            this.notifyIcon1.Text = form.NazevUdalosti + "\n" + doba;

            if (test.Days == 0 && test.Hours == 0 && test.Minutes == 0 && test.Seconds == 0)
            {
                timer1.Stop();
                this.Close();
            }
        }

        /// <summary>
        /// Událost vyvolaná po zavření okna
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MinimalForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            form.Show();
            form.WindowState = FormWindowState.Normal;
        }

        /// <summary>
        /// Událost obsluhující menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ObsluhaMenu(object sender, EventArgs e)
        {
            if (sender == konecToolStripMenuItem)
                form.Close();

            else if (sender == obnovitToolStripMenuItem)
            {
                    this.Show();
                    this.WindowState = FormWindowState.Normal;
                    obnovitToolStripMenuItem.Visible = false;
            }
            else if (sender == vždyNahořeToolStripMenuItem1)
            {
                ToolStripMenuItem vzdy = vždyNahořeToolStripMenuItem1;

                if (vzdy.Checked)
                {
                    vzdy.Checked = false;
                    this.TopMost = false;
                }
                else
                {
                    vzdy.Checked = true;
                    this.TopMost = true;
                }
            }
        }

        /// <summary>
        /// Událost vyvolaná při dvojkliku na notifikační ikonu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.Show();
                this.WindowState = FormWindowState.Normal;
            }
        }
    }
}
