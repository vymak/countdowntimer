﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Media;
using OdpocetCasu.Classes;
using System.IO;

namespace OdpocetCasu
{
    /// <summary>
    /// Třída obsahující hlavní okno aplikace
    /// </summary>
    internal partial class Form1 : Form
    {
        /// <summary>
        /// Zadané datum
        /// </summary>
        private DateTime zadaneDatum;

        /// <summary>
        /// Přehrávač alarmu při dokončení odpočtu
        /// </summary>
        private SoundPlayer prehravac;

        /// <summary>
        /// Indikuje zda-li byla již načtená data
        /// </summary>
        internal bool NactenaData { get; set; }

        /// <summary>
        /// Vrátí zadané datum od uživatele
        /// </summary>
        internal DateTime PropertyZadaneDatum
        {
            get { return this.zadaneDatum; }
        }

        /// <summary>
        /// Vrátí název události
        /// </summary>
        internal string NazevUdalosti { get; set; }

        /// <summary>
        /// Seznam událostí
        /// </summary>
        internal Dictionary<DateTime, string> ListUdalosti;

        /// <summary>
        /// Property indikující zda se mají data ukládat automaticky
        /// </summary>
        internal bool UkladatAutomaticky { get; set; }

        /// <summary>
        /// Konstruktor pro vytvoření hlavního okna aplikace
        /// </summary>
        internal Form1()
        {
            InitializeComponent();
            NactenaData = false;
            DateTime casNyni = DateTime.Now;
            prehravac = new SoundPlayer(Properties.Resources.alarm);
            UkladatAutomaticky = true;
            timer1.Start();
            timer1.Enabled = true;

            monthCalendar1.MinDate = casNyni;
            ListUdalosti = new Dictionary<DateTime, string>();

            UpdateCheck();

            if (casNyni.Hour != 23)
            {
                hodinyVyber.SelectedIndex = 23 - casNyni.Hour - 1;
                minutyVyber.SelectedIndex = 59;
                vterinyVyber.SelectedIndex = 59;
            }
            else
            {
                NastavDatumZVyberu(casNyni.AddDays(1));
                hodinyVyber.SelectedIndex = 23;
            }

            CheckIfDataExist();
            toolStripStatusLabel1.Text = SystemInformation.UserName.ToString();
        }

        /// <summary>
        /// Kontroluje dostupnost nové verze
        /// </summary>
        private void UpdateCheck()
        {
            if (UpdateChecker.IsNewVersionAvailible())
            {
                DialogResult res = MessageBox.Show("K dispozici je novější verze programu\n\nPřejete si aktualizovat program na nejnovější verzy?", "Dostupná aktualizace", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == System.Windows.Forms.DialogResult.Yes)
                {
                    DialogResult soubor = saveFileDialog1.ShowDialog();

                    if (soubor == System.Windows.Forms.DialogResult.OK)
                    {
                        if (!UpdateChecker.DownloadNewVersion(saveFileDialog1.FileName))
                            MessageBox.Show("Během aktualizace došlo k neznámé chybě! Prosím zkuste program spustit znovu", "Chyba aktualizace", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        /// <summary>
        /// Metoda kontrolující zda-li existují uložená data
        /// </summary>
        private void CheckIfDataExist()
        {
            if (File.Exists("data.bin"))
            {
                DialogResult res = MessageBox.Show("Byly nalezena Vaše uložená data. Přejete si tyto data načíst?", "Načíst data?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == System.Windows.Forms.DialogResult.Yes)
                {
                    ListUdalosti = EventManager.LoadEventsFromFile();
                    NactenaData = true;
                }
                else if (res == System.Windows.Forms.DialogResult.No)
                {
                    NactenaData = true;
                }
            }
        }

        /// <summary>
        /// Událost obsluhující chechboxy
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckBoxManage(object sender, EventArgs e)
        {
            if (sender == timerChceckbox)
            {
                if (timerChceckbox.Checked)
                {
                    timer1.Stop();
                    timer1.Enabled = false;
                    kontrolaTimer.Start();
                }
                else
                {
                    timer1.Start();
                    timer1.Enabled = true;
                    kontrolaTimer.Stop();
                }
            }
            else if (sender == topCheckbox)
            {
                if (topCheckbox.Checked)
                    this.TopMost = true;
                else
                    this.TopMost = false;
            }
        }

        /// <summary>
        /// Událost vyvolaná při změně datumu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZmenaData(object sender, DateRangeEventArgs e)
        {
            if (!timer1.Enabled)
            {
                timer1.Start();
                timer1.Enabled = true;
            }

            if (hodinyVyber.SelectedIndex != -1 && minutyVyber.SelectedIndex != -1 && vterinyVyber.SelectedIndex != -1)
                zadaneDatum = new DateTime(monthCalendar1.SelectionStart.Year, monthCalendar1.SelectionStart.Month, monthCalendar1.SelectionStart.Day, int.Parse(hodinyVyber.Text), int.Parse(minutyVyber.Text), int.Parse(vterinyVyber.Text));

            zbyvajiciCas.Visible = true;
            label3.Visible = true;
            label4.Visible = true;
            ciloveDatum.Visible = true;
            toolStripStatusLabel3.Visible = true;
        }

        /// <summary>
        /// Událost obsluhující tick timeru
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer1_Tick(object sender, EventArgs e)
        {
            DateTime nyni = DateTime.Now;

            aktualniDatum.Text = nyni.ToString();
            ciloveDatum.Text = zadaneDatum.ToString();

            TimeSpan rozdil = zadaneDatum - nyni;

            if (rozdil.TotalSeconds < 0)
            {
                while (rozdil.TotalSeconds <= 0)
                {
                    try
                    {
                        hodinyVyber.SelectedIndex = hodinyVyber.SelectedIndex - 1;
                        rozdil = zadaneDatum - DateTime.Now;
                    }
                    catch (ArgumentException)
                    {
                        break;
                    }
                }
            }

            zbyvajiciCas.Text = null;
            toolStripStatusLabel3.Text = null;

            string doba = EventManager.CreateTimeSpanString(rozdil);
            zbyvajiciCas.Text = doba;

            toolStripStatusLabel3.Text = "Celkově: ";
            toolStripStatusLabel3.Text += Math.Round(rozdil.TotalDays, 3).ToString() + " dní | ";
            toolStripStatusLabel3.Text += Math.Round(rozdil.TotalHours, 3).ToString() + " hodin | ";
            toolStripStatusLabel3.Text += Math.Round(rozdil.TotalMinutes, 2).ToString() + " minut | ";
            toolStripStatusLabel3.Text += Math.Round(rozdil.TotalSeconds, 2).ToString() + " vteřin";


            if (rozdil.Days == 0 && rozdil.Hours == 0 && rozdil.Minutes == 0 && rozdil.Seconds == 0)
            {
                timer1.Stop();
                timer1.Enabled = false;

                if (playSound.Checked)
                    prehravac.Play();

                MessageBox.Show("Čas události " + nazevUdalosti.Text + " uplynul", "Odpočet hotový", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Událost obsluhující up and down listy nastavující čas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NastaveniCasu(object sender, EventArgs e)
        {
            ZmenaData(sender, null);
        }

        /// <summary>
        /// Událost obsluhující chování menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuManage(object sender, EventArgs e)
        {
            if (sender == konecToolStripMenuItem)
            {
                Application.Exit();
            }
            else if (sender == oProgramuToolStripMenuItem)
            {
                new About().Show();
            }
            else if (sender == aktualizovatAutomatickyToolStripMenuItem)
            {
                if (aktualizovatAutomatickyToolStripMenuItem.Checked)
                {
                    aktualizovatAutomatickyToolStripMenuItem.Checked = false;
                    UkladatAutomaticky = false;
                }
                else
                {
                    aktualizovatAutomatickyToolStripMenuItem.Checked = true;
                    UkladatAutomaticky = true;
                }
            }
            else if (sender == odeslatFeedBackToolStripMenuItem)
            {
                new SendFeedback().Show();
            }
        }

        /// <summary>
        /// Metoda pro vkládání nových datumů do seznamu
        /// </summary>
        private void VlozeniUdalosti()
        {
            if (!ListUdalosti.Keys.Contains(zadaneDatum))
            {
                ListUdalosti.Add(zadaneDatum, nazevUdalosti.Text);

                if (UkladatAutomaticky)
                    EventManager.SaveEvents(ListUdalosti);
            }
            else
                MessageBox.Show("Zadané datum a čas již je ve Vašem seznamu", "Informace", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// Událost vyvolaná při kliknutí na tlačítko vložit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VlozitButtonClick(object sender, EventArgs e)
        {
            if (nazevUdalosti.Text.Length != 0)
            {
                if (File.Exists("data.bin") && !NactenaData)
                {
                    DialogResult res = MessageBox.Show("Byl nalezen soubor obsahující Vaše dřívější data!\n\nPřejete si před uložením nového datumu načíst Vaše data?", "Nalezena data", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (res == System.Windows.Forms.DialogResult.Yes)
                    {
                        ListUdalosti = EventManager.LoadEventsFromFile();
                        NactenaData = true;
                        VlozeniUdalosti();
                    }
                }
                else
                {
                    VlozeniUdalosti();
                }
            }
            else
            {
                MessageBox.Show("Před uložením prosím zadejte název odpočtu", "Chyba vkládání", MessageBoxButtons.OK, MessageBoxIcon.Information);
                nazevUdalosti.Focus();
            }
        }

        /// <summary>
        /// Událost vyvolaná při kliknutí na tlačítko správce dat
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VyberDatum_Click(object sender, EventArgs e)
        {
            vlozButton.Enabled = false;
            vyberDatum.Enabled = false;
            new SeznamDatumu(ListUdalosti, this).Show();
        }

        /// <summary>
        /// Nastavení data
        /// </summary>
        /// <param name="datum">Datum</param>
        /// <returns>Bool</returns>
        internal bool NastavDatumZVyberu(DateTime datum)
        {
            try
            {
                zadaneDatum = new DateTime(datum.Year, datum.Month, datum.Day, datum.Hour, datum.Minute, datum.Second);
                monthCalendar1.SetDate(datum);
                monthCalendar1.Refresh();
                hodinyVyber.SelectedIndex = 23 - datum.Hour;
                minutyVyber.SelectedIndex = 59 - datum.Minute;
                vterinyVyber.SelectedIndex = 59 - datum.Second;
                return true;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Událost vyvolaná při změně stavu okna
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                new MinimalForm(this).Show();
                this.Hide();
            }
        }

        /// <summary>
        /// Událost vyvolaná při změně názvu události
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NazevUdalosti_TextChanged(object sender, EventArgs e)
        {
            NazevUdalosti = nazevUdalosti.Text;

            if (nazevUdalosti.Text.Length != 0)
            {
                this.nazevNadpis.Text = nazevUdalosti.Text;
                this.nazevNadpis.Visible = true;
                this.vlozButton.Enabled = true;
            }
            else
            {
                this.nazevNadpis.Visible = false;
                this.vlozButton.Enabled = false;
            }
        }

        /// <summary>
        /// Událost vyvolaná při změně posuvníku intervalu aktualizace
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NastaveniIntervalu_Scroll(object sender, EventArgs e)
        {
            this.timer1.Interval = nastaveniIntervalu.Value;
            this.intervalLabel.Text = nastaveniIntervalu.Value.ToString() + " ms";
            this.intervalLabel.Visible = true;
        }

        /// <summary>
        /// Událost ticku timeru pro kontrolu zda-li je hlavní timer pozastaven
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void kontrolaTimer_Tick(object sender, EventArgs e)
        {
            if (!timer1.Enabled)
            {
                TimeSpan rozdil = zadaneDatum - DateTime.Now;

                if (rozdil.TotalSeconds <= 60)
                    timerChceckbox.Checked = false;
            }
        }

        private void LabelMenuClick(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (sender == countdownLabel)
            { 
            
            }
            else if (sender == stopwatchLabel)
            { 
            
            }
            else if (sender == alarmLabel)
            { 
            
            }
        }
    }
}
