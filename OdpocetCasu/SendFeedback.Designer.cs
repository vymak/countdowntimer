﻿namespace OdpocetCasu
{
    partial class SendFeedback
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SendFeedback));
            this.label1 = new System.Windows.Forms.Label();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.typComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.zpravaTextBox = new System.Windows.Forms.TextBox();
            this.odeslatButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "E-mailová adresa:";
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(15, 34);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(276, 20);
            this.emailTextBox.TabIndex = 1;
            this.emailTextBox.TextChanged += new System.EventHandler(this.ZmenaTextu);
            // 
            // typComboBox
            // 
            this.typComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typComboBox.FormattingEnabled = true;
            this.typComboBox.Items.AddRange(new object[] {
            "Nahlášení chyby v aplikaci",
            "Návrh na vylepšení programu",
            "Jiné ..."});
            this.typComboBox.Location = new System.Drawing.Point(15, 86);
            this.typComboBox.Name = "typComboBox";
            this.typComboBox.Size = new System.Drawing.Size(276, 21);
            this.typComboBox.TabIndex = 2;
            this.typComboBox.SelectedIndexChanged += new System.EventHandler(this.ZmenaTextu);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Typ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Zpráva:";
            // 
            // zpravaTextBox
            // 
            this.zpravaTextBox.Location = new System.Drawing.Point(15, 144);
            this.zpravaTextBox.Multiline = true;
            this.zpravaTextBox.Name = "zpravaTextBox";
            this.zpravaTextBox.Size = new System.Drawing.Size(276, 140);
            this.zpravaTextBox.TabIndex = 5;
            this.zpravaTextBox.TextChanged += new System.EventHandler(this.ZmenaTextu);
            // 
            // odeslatButton
            // 
            this.odeslatButton.Enabled = false;
            this.odeslatButton.Location = new System.Drawing.Point(216, 292);
            this.odeslatButton.Name = "odeslatButton";
            this.odeslatButton.Size = new System.Drawing.Size(75, 23);
            this.odeslatButton.TabIndex = 6;
            this.odeslatButton.Text = "Odeslat";
            this.odeslatButton.UseVisualStyleBackColor = true;
            this.odeslatButton.Click += new System.EventHandler(this.OdeslatButton_Click);
            // 
            // SendFeedback
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(303, 327);
            this.Controls.Add(this.odeslatButton);
            this.Controls.Add(this.zpravaTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.typComboBox);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SendFeedback";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Odeslat Feedback";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button odeslatButton;
        internal System.Windows.Forms.TextBox emailTextBox;
        internal System.Windows.Forms.ComboBox typComboBox;
        internal System.Windows.Forms.TextBox zpravaTextBox;
    }
}