﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using OdpocetCasu.Classes;

namespace OdpocetCasu
{
    /// <summary>
    /// Třída obsluhující form se seznamem událostí
    /// </summary>
    internal partial class SeznamDatumu : Form
    {
        /// <summary>
        /// Seznam událostí
        /// </summary>
        private Dictionary<DateTime, string> udalosti;

        /// <summary>
        /// Proměnná s instancí hlavního okna
        /// </summary>
        private Form1 form;

        /// <summary>
        /// Property s aktuálním indexem řádku
        /// </summary>
        private int currentIndex { get; set; }

        /// <summary>
        /// Property s posledním platným datumem
        /// </summary>
        private DateTime lastValidDate { get; set; }

        /// <summary>
        /// Vytvoření instance okna se seznamem
        /// </summary>
        /// <param name="seznam">Uložený seznam událostí</param>
        /// <param name="form">Instance formu</param>
        internal SeznamDatumu(Dictionary<DateTime, string> seznam, Form1 form)
        {
            InitializeComponent();
            udalosti = seznam;
            this.form = form;
            UpdateSeznamDatumu();
            autoSaveChange.Checked = this.form.UkladatAutomaticky;
            timerKontrola.Start();
            currentIndex = 0;
        }

        /// <summary>
        /// Nahrání položek do seznamu
        /// </summary>
        private void UpdateSeznamDatumu()
        {
            dataGridView1.Rows.Clear();

            var serazeneUdalosti = udalosti.OrderBy(x => x.Key);
            int i = 0;

            foreach (KeyValuePair<DateTime, string> item in serazeneUdalosti)
            {
                TimeSpan rozdil = item.Key - DateTime.Now;

                string platneDatum = "0";

                if (rozdil.TotalSeconds >= 0)
                {
                    if (i == 0)
                    {
                        lastValidDate = item.Key;
                        i++;
                    }

                    platneDatum = "1";
                }
                
                dataGridView1.Rows.Add(item.Value, item.Key, "Smazat", platneDatum);
            }

            foreach (DataGridViewRow radek in dataGridView1.Rows)
            {
                DateTime cas = (DateTime)radek.Cells[1].Value;

                if (form.PropertyZadaneDatum == cas)
                    currentIndex = radek.Index;


                if (radek.Cells[3].Value.ToString() == "0")
                {
                    radek.DefaultCellStyle.ForeColor = Color.Red;
                }
            }
        }

        /// <summary>
        /// Událost vyvolaná při kliknutí na buňku v seznamu událostí
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                if (e.ColumnIndex != 2)
                {
                    if (dataGridView1[3, e.RowIndex].Value.ToString() == "1")
                    {
                        try
                        {
                            form.NastavDatumZVyberu((DateTime)dataGridView1[1, e.RowIndex].Value);
                            form.nazevNadpis.Text = (string)dataGridView1[0, e.RowIndex].Value;
                            form.NazevUdalosti = form.nazevNadpis.Text = (string)dataGridView1[0, e.RowIndex].Value;
                            form.nazevNadpis.Visible = true;

                            if (!form.timer1.Enabled)
                            {
                                form.timer1.Start();
                                form.timer1.Enabled = true;
                            }
                        }
                        catch (Exception)
                        {
                            MessageBox.Show("Během vykonávání došlo k chybě", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else 
                    {
                        TimeSpan tmp = (DateTime)dataGridView1[1, e.RowIndex].Value - DateTime.Now;
                        MessageBox.Show("Zvolené datum již uplynulo!\n\nDatum uplynulo před:\n" + tmp.ToString(), "Datum uplynulo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    udalosti = EventManager.RemoveRowByIndex(dataGridView1[1, e.RowIndex].Value.ToString(), udalosti);
                    UpdateSeznamDatumu();
                    UkladaniNaDisk();
                }
            }
        }

        /// <summary>
        /// Událost vyvolaná při kliknutí na tlačítko uložit
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveButton_Click(object sender, EventArgs e)
        {
            EventManager.SaveEvents(udalosti);
        }

        /// <summary>
        /// Událost při kliknutí na tlačítko načíst
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadButton_Click(object sender, EventArgs e)
        {
            if (File.Exists("data.bin"))
            {
                udalosti = EventManager.LoadEventsFromFile();
                form.NactenaData = true;
                UpdateSeznamDatumu();
            }
            else
            {
                MessageBox.Show("Data nebyla nalezena", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Událost vyvolaná při zavírání okna se seznamem
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SeznamDatumu_FormClosing(object sender, FormClosingEventArgs e)
        {
            form.ListUdalosti = udalosti;
            form.vlozButton.Enabled = true;
            form.vyberDatum.Enabled = true;
        }

        /// <summary>
        /// Ukládání na disk v případě že je povoleno automatické ukládání
        /// </summary>
        private void UkladaniNaDisk()
        {
            if (autoSaveChange.Checked)
                EventManager.SaveEvents(udalosti);
        }

        /// <summary>
        /// Událost timeru pro kontrolu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerKontrola_Tick(object sender, EventArgs e)
        {
            if (lastValidDate != new DateTime(1, 1, 1, 0, 0, 0))
            {
                TimeSpan rozdil = lastValidDate - DateTime.Now;

                if (rozdil.TotalSeconds <= 0)
                    UpdateSeznamDatumu();
            }
        }

        private void autoSaveChange_CheckedChanged(object sender, EventArgs e)
        {
            this.form.aktualizovatAutomatickyToolStripMenuItem.Checked = autoSaveChange.Checked;
            this.form.UkladatAutomaticky = autoSaveChange.Checked;
        }
    }
}
