﻿using OdpocetCasu.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace OdpocetCasu
{
    /// <summary>
    /// Třída obluhující form s posíláním Feedbacku
    /// </summary>
    public partial class SendFeedback : Form
    {
        /// <summary>
        /// Konstruktor pro vytvoření nové instance SendFeedback
        /// </summary>
        public SendFeedback()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Událost vyvolaná při změně textu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZmenaTextu(object sender, EventArgs e)
        {
            if (emailTextBox.Text.Length != 0 && zpravaTextBox.Text.Length != 0 && typComboBox.SelectedIndex != -1)
            {
                if (IsEmailValid())
                    odeslatButton.Enabled = true;
                else
                    odeslatButton.Enabled = false;
            }
            else
            {
                odeslatButton.Enabled = false;
            }
        }

        /// <summary>
        /// Validace emailové adresy
        /// </summary>
        /// <returns></returns>
        private bool IsEmailValid()
        {
            Match match = Regex.Match(emailTextBox.Text, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            if (match.Success)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Událost vyvolaná při kliknutí na tlačítko odeslat
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OdeslatButton_Click(object sender, EventArgs e)
        {
        }
    }
}
